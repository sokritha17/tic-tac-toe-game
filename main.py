import time
from player import RandomComputerPlayer
from game import TicTaeToe


def play(game, _x_player, _o_player, print_game=True):
    if print_game:
        game.print_board_nums()
        print('')

    # Determine which player start first
    letter = _o_player.letter
    while game.empty_square():
        if letter == 'O':
            square = _o_player.get_move(game)
        else:
            square = _x_player.get_move(game)
        if game.make_move(square, letter):
            if print_game:
                print(letter + ' makes a move to square {}'.format(square))
                game.print_board()
                print('')
            if game.current_winner:
                if print_game:
                    print(letter + ' wins!')
                return letter
            letter = 'O' if letter == 'X' else 'X'
        time.sleep(.8)
    if print_game:
        print('It\' a tie!')

if __name__ == '__main__':
    data = []
    x_player = RandomComputerPlayer('X')
    o_player = RandomComputerPlayer('O')
    t = [TicTaeToe() for i in range(2)]
    for i in range(2):
        print('\nGame {}: '.format(i+1))
        print('-------------------------------')
        play(t[i], x_player, o_player, print_game=True)
        data.append(t[i].board_state)
    print('----------------------------')
    print('collection data: {}'.format(data))


