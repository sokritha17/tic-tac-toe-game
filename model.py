# import torch
# from torch import optim
# import numpy as np
# import torch.nn as nn
#
#
# class Model(nn.Module):
#     def __init__(self):
#         super(Model, self).__init__()
#         self.a = nn.Parameter(torch.tensor(0, dtype=torch.float32))
#         self.b = nn.Parameter(torch.tensor(0, dtype=torch.float32))
#
#     def forward(self, _x):
#         return self.a * _x + self.b
#
#
# # Create data
# m = 30
# x = np.random.randn(m)
# _a = 10
# _b = -5
#
# y = _a * x + _b
#
# # Train
# x = torch.tensor(x, dtype=torch.float32)
# y = _a * x + _b
#
# model = Model()
# opt = optim.SGD(model.parameters(), lr=1)
#
# for i in range(10):
#     opt.zero_grad()
#
#     h = model(x)
#     cost = torch.pow(y-h, 2).sum()/m
#
#     print('Iteration: {}'.format(i+1))
#     print('Cost: {}'.format(cost.item()))
#     print('a: {}, b: {}'.format(model.a.item(), model.b.item()))
#
#     cost.backward()
#     opt.step()
#
#
#
