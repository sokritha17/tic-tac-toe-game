import math


class TicTaeToe:
    def __init__(self):
        self.board_state = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        self.current_winner = None

    def print_board(self):
        for rows in [self.board_state[i] for i in range(3)]:
            print(rows)

    def make_move(self, ind_square, letter):
        if self.board_state[self.convert_square_row(ind_square)][self.convert_square_col(ind_square)] == 0:
            if letter == 'O':
                self.board_state[self.convert_square_row(ind_square)][self.convert_square_col(ind_square)] = 1
            elif letter == 'X':
                self.board_state[self.convert_square_row(ind_square)][self.convert_square_col(ind_square)] = 2

            if self.winner(ind_square, letter):
                self.current_winner = letter
            return True
        return False

    @staticmethod
    def print_board_nums():
        number_board = [[str(i) for i in range(j * 3, (j + 1) * 3)] for j in range(3)]
        for row in number_board:
            print('| ' + ' | '.join(row) + ' |')

    @staticmethod
    def convert_square_col(ind_square):
        return ind_square % 3

    @staticmethod
    def convert_square_row(ind_square):
        return math.floor(ind_square / 3)

    @staticmethod
    def convert_letter(letter):
        if letter == 'O':
            return 1
        elif letter == 'X':
            return 2

    def find_first_diagonal(self):
        diagonal1 = []
        for ind_row, row in enumerate(self.board_state):
            if ind_row == 0:
                diagonal1.append(row[0])
            elif ind_row == 1:
                diagonal1.append(row[1])
            elif ind_row == 2:
                diagonal1.append(row[2])
        return diagonal1

    def find_second_diagonal(self):
        diagonal2 = []
        for ind_row, row in enumerate(self.board_state):
            if ind_row == 0:
                diagonal2.append(row[2])
            elif ind_row == 1:
                diagonal2.append(row[1])
            elif ind_row == 2:
                diagonal2.append(row[0])
        return diagonal2

    def winner(self, ind_square, letter):
        # checks the row
        row = self.board_state[self.convert_square_row(ind_square)]
        # checks the column
        cols = [self.board_state[i][self.convert_square_col(ind_square)] for i in range(3)]
        # win cases:
        # 1. checks horizontal line
        if all([s == self.convert_letter(letter) for s in row]):
            return True
        # 2. checks vertical line
        if all([s == self.convert_letter(letter) for s in cols]):
            return True
        # 3. checks diagonal
        if ind_square % 2 == 0:
            diagonal1 = self.find_first_diagonal()
            if all([s == self.convert_letter(letter) for s in diagonal1]):
                return True
            diagonal2 = self.find_second_diagonal()
            if all([s == self.convert_letter(letter) for s in diagonal2]):
                return True
        return False

    def empty_square(self):
        for row in self.board_state:
            for col in row:
                if col == 0:
                    return True
        return False

    def num_empty_squares(self):
        return sum([i.count(0) for i in self.board_state])

    def available_moves(self):
        list_available_move = []
        for ind_row, row in enumerate(self.board_state):
            if ind_row == 0:
                for ind_col, col in enumerate(row):
                    if col == 0:
                        list_available_move.append(ind_col)
            elif ind_row == 1:
                for ind_col, col in enumerate(row):
                    if col == 0:
                        if ind_col == 0:
                            list_available_move.append(3)
                        elif ind_col == 1:
                            list_available_move.append(4)
                        elif ind_col == 2:
                            list_available_move.append(5)
            elif ind_row == 2:
                for ind_col, col in enumerate(row):
                    if col == 0:
                        if ind_col == 0:
                            list_available_move.append(6)
                        elif ind_col == 1:
                            list_available_move.append(7)
                        elif ind_col == 2:
                            list_available_move.append(8)
        return list_available_move
